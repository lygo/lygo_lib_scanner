# LyGo Scanner

Scanner application 

This folder contains packages that uses external programs or binaries.
For example:
- tesseract
- imagemagick
- ghostscript
- ...

## How to Use

To use just call:

`go get -u bitbucket.org/lygo/lygo_lib_scanner`

# Dependencies

```
 go get -u bitbucket.org/lygo/lygo_commons
 go get -u bitbucket.org/lygo/lygo_ext_logs
 go get -u bitbucket.org/lygo/lygo_ext_nlp
```

## Tesseract
Library: [https://github.com/otiai10/gosseract](https://github.com/otiai10/gosseract)

### Install

- [tesseract-ocr](https://github.com/tesseract-ocr/tesseract/wiki), including library and headers
- Go: `go get -t github.com/otiai10/gosseract`
 
## Imagemagick & Ghostscript

Imagemagick uses ghostscript fonts.
To install and configure Go: https://github.com/gographics/imagick#build-tags

### Install

 - [imagemagik](https://imagemagick.org/script/download.php) and Ghostscript
 - Go: `go get -t gopkg.in/gographics/imagick.v3/imagick`

##Issues

### "Invalid Flag" error

Whitelist the -Xpreprocessor flag in your environment.

`export CGO_CFLAGS_ALLOW='-Xpreprocessor'`

#### "Invalid Flag Error" in IntelliJ test
In IntelliJ or GoLand you can edit configuration adding an **Environment** Variable: 
`CGO_CFLAGS_ALLOW=-Xpreprocessor`


### Versioning

Sources are versioned using git tags:

```
git tag v0.1.5
git push origin v0.1.5
```



