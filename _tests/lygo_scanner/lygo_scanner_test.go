package lygo_scanner

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_stopwatch"
	"bitbucket.org/lygo/lygo_lib_scanner/_tests"
	"bitbucket.org/lygo/lygo_lib_scanner/lygo_lib_scanner"
	"fmt"
	"testing"
)

func TestSimple(t *testing.T) {

	_tests.InitContext()

	file := lygo_paths.WorkspacePath("./rules/rule.json")
	rule, err := lygo_io.ReadTextFromFile(file)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	paramsArray := new(lygo_lib_scanner.ScannerConfigArray)
	paramsArray.Parse(rule)

	scanner := lygo_lib_scanner.NewScanner(lygo_paths.GetWorkspacePath() + "/tmp")
	response, err := scanner.ReadDocuments(lygo_paths.WorkspacePath("./documents.pdf"), true, paramsArray)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// postprocessor
	postprocessor := lygo_lib_scanner.NewScannerVM(lygo_paths.WorkspacePath("./rules/postprocessor.js"))
	postprocessor.Workspace = lygo_paths.Concat(lygo_paths.GetWorkspacePath(), "logging")
	doc := lygo_json.Stringify(response)
	context := map[string]interface{}{
		"document": doc,
	}
	postresult, err := postprocessor.Eval(context)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("POST-RESPONSE:", postresult)

	fmt.Println("ELAPSED MS:", response.ElapsedMs)
	fmt.Println("DOCS:", len(response.Documents))
	for _, doc := range response.Documents {
		fmt.Println("\t", "UID:", doc.Uid())
		fmt.Println("\t", "PAGES:", len(doc.Pages))
		fmt.Println("\t", "---------")
	}

	fmt.Println()

	fmt.Println("REMOVING ALL...")
	err = scanner.Remove()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
}

func TestLoop(t *testing.T) {
	_tests.InitContext()

	file := lygo_paths.WorkspacePath("./rules/rule.json")
	rule, err := lygo_io.ReadTextFromFile(file)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	paramsArray := new(lygo_lib_scanner.ScannerConfigArray)
	paramsArray.Parse(rule)

	scanner := lygo_lib_scanner.NewScanner(lygo_paths.GetWorkspacePath() + "/tmp")
	scanner.Async = false

	for i := 0; i < 1000; i++ {
		watch := lygo_stopwatch.New()
		watch.Start()

		_, err := scanner.ReadDocuments(lygo_paths.WorkspacePath("./one_page.pdf"), true, paramsArray)
		if nil != err {
			t.Error(err)
			t.FailNow()
		}
		err = scanner.Remove()
		if nil != err {
			t.Error(err)
			t.FailNow()
		}

		watch.Stop()
		fmt.Println(i, watch.Milliseconds())
	}

}
