package _tests

import (
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_logs"
	"fmt"
)

func InitContext() {

	var root = lygo_paths.Absolute("../")
	fmt.Println("WORKSPACE:", root)

	lygo_paths.SetWorkspaceParent(root)

	lygo_ext_logs.SetLevel(lygo_ext_logs.LEVEL_TRACE)
	lygo_ext_logs.SetOutput(lygo_ext_logs.OUTPUT_FILE)
	lygo_ext_logs.Info(
		lygo_ext_logs.LogContext{
			Caller: "initializer.InitContext()",
			Data:   "This is the INITIALIZATION context. LOG Level: " + lygo_conv.ToString(lygo_ext_logs.GetLevel()),
		},
		"initialized")

}
