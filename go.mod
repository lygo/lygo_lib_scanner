module bitbucket.org/lygo/lygo_lib_scanner

go 1.14

require (
	bitbucket.org/lygo/lygo_commons v0.1.10
	bitbucket.org/lygo/lygo_ext_logs v0.1.1
	bitbucket.org/lygo/lygo_ext_nlp v0.1.2
	bitbucket.org/lygo/lygo_ext_scripting v0.1.4
	github.com/otiai10/gosseract v2.2.1+incompatible
	github.com/otiai10/mint v1.3.1 // indirect
	golang.org/x/net v0.0.0-20200625001655-4c5254603344 // indirect
	golang.org/x/sys v0.0.0-20200625212154-ddb9806d33ae // indirect
	gopkg.in/gographics/imagick.v3 v3.3.0
)
