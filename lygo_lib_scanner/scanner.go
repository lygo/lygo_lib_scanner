package lygo_lib_scanner

import (
	"bitbucket.org/lygo/lygo_commons/lygo_async"
	"bitbucket.org/lygo/lygo_commons/lygo_conv"
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_rnd"
	"bitbucket.org/lygo/lygo_commons/lygo_stopwatch"
	"bitbucket.org/lygo/lygo_commons/lygo_strings"
	"bitbucket.org/lygo/lygo_ext_logs"
	"bitbucket.org/lygo/lygo_lib_scanner/lygo_images"
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

var (
	EmptyDocumentError = errors.New("bad_document_error")
)

type Scanner struct {
	Id               string
	Workspace        string
	ConversionFormat string
	ConversionDpi    float64
	Async            bool

	//-- p r i v a t e --//
	mutex      sync.Mutex
	threadPool *lygo_async.ConcurrentPool
}

type ScannerTask struct {
	Processed []*ScannerDocument
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewScanner(optWorkspace ...interface{}) *Scanner {
	id := lygo_rnd.UuidTimestamp() // get new timestamped UUID

	// init root
	tmpRoot := lygo_paths.GetTempRoot()
	if len(optWorkspace) == 1 {
		if b, v := lygo_conv.IsString(optWorkspace[0]); b {
			tmpRoot = v
		}
	}
	workspace := filepath.Join(tmpRoot, id)

	response := &Scanner{
		Id:               id,
		Workspace:        workspace,
		ConversionFormat: "jpg",
		ConversionDpi:    200,
		Async:            false,
	}

	// init workspace
	_ = lygo_paths.Mkdir(workspace)

	return response
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Scanner) SetThreadPool(pool *lygo_async.ConcurrentPool) {
	if nil != pool {
		instance.threadPool = pool
	}
}

func (instance *Scanner) SplitDocuments(fileName string, oneDocumentPerPage bool) ([][]string, error) {
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			msg := lygo_strings.Format("Scanner.SplitDocuments ERROR: %s", r)
			lygo_ext_logs.Error(msg)
			fmt.Println("Scanner.SplitDocuments ERROR: ", msg)
		}
	}()

	response := make([][]string, 0)

	// copy original to workspace
	original, err := instance.copyToWorkspace(fileName)
	if nil == err {
		// try to convert file if is PDF
		var isConverted bool
		var pages []string
		isConverted, pages, err = instance.convert(original)

		if nil == err {
			if !isConverted {
				var target string
				target, err = instance.copyToPage(original)
				pages = append(pages, target)
			}

			if nil == err {
				if oneDocumentPerPage {
					// many documents: one page generate one document
					for _, page := range pages {
						response = append(response, []string{page})
					}
				} else {
					// single multi-page document
					response = append(response, pages)
				}
			}

			// pages should not be zero, at least one page
			if len(response) == 0 {
				err = EmptyDocumentError
			}
		}
	}

	return response, err
}

// Read a file that can be a multi-pages single document or single-page documents, marching with all available
// configurations to detect best matching.
// In case of many documents with a single page, the output respect document order.
// This procedure test all documents with all configurations
func (instance *Scanner) ReadDocuments(fileName string, oneDocumentPerPage bool,
	configArray *ScannerConfigArray) (*ScannerResponse, error) {
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			msg := lygo_strings.Format("Scanner.ReadDocuments ERROR: %s", r)
			lygo_ext_logs.Error(msg)
			fmt.Println("Scanner.ReadDocuments ERROR: ", msg)
		}
	}()

	stopwatch := lygo_stopwatch.New()
	stopwatch.Start()

	workConfig := configArray.Clone()

	response := new(ScannerResponse)
	response.Uid = instance.Id
	response.Original = fileName
	response.Params = configArray
	response.Documents = make([]*ScannerDocument, 0)
	response.ElapsedMs = 0

	documents, err := instance.SplitDocuments(fileName, oneDocumentPerPage)
	if nil == err {

		for _, documentPages := range documents {
			var bestScore float32
			var bestDocument *ScannerDocument
			var emptyDocument *ScannerDocument
			var fallbackDocument *ScannerDocument
			emptyDocument = NewScannerDocumentEmpty(instance.Workspace, documentPages, *new(ScannerConfig))

			// loop on all configurations to get the best match
			var group sync.WaitGroup
			task := new(ScannerTask)
			task.Processed = make([]*ScannerDocument, 0)

			for _, config := range workConfig.Items() {
				//instance.pool().Run(func() {
				//instance.goScan(task, documentPages, config)
				//})
				if instance.Async {
					// async fill list
					group.Add(1)
					go instance.goScanGroup(&group, task, documentPages, config)
				} else {
					instance.goScanGroup(nil, task, documentPages, config)
				}
			}

			// wait elaboration
			if instance.Async {
				group.Wait()
			}

			// assign best document matching or fallback
			for _, document := range task.Processed {
				fallbackDocument = document
				if len(document.Pages) > 0 {
					bestItem := document.BestJobArea()
					if nil != bestItem && nil != bestItem.Nlp {
						if bestItem.Nlp.Score > bestScore {
							bestScore = bestItem.Nlp.Score
							bestDocument = document
						}
					}
				}
			}

			if nil != bestDocument {
				// assign a document to response
				response.Documents = append(response.Documents, bestDocument)
				// matched configuration cannot be removed
				// workConfig.Remove(bestDocument.Params)
			} else if nil != fallbackDocument {
				// not a best document, but something with parameters and data
				response.Documents = append(response.Documents, fallbackDocument)
			} else {
				// document not matching models passed in configuration "configArray"
				response.Documents = append(response.Documents, emptyDocument)
			}
		}
	}

	stopwatch.Stop()
	response.ElapsedMs = stopwatch.Milliseconds()

	return response, err
}

// Match a file that can be a multi-pages single document or single-page documents, with a single configuration.
// This is useful to detect which document best match with passed configuration
func (instance *Scanner) MatchDocuments(fileName string, oneDocumentPerPage bool,
	config *ScannerConfig) (*ScannerResponse, error) {
	defer func() {
		if r := recover(); r != nil {
			// recovered from panic
			msg := lygo_strings.Format("Scanner.MatchDocuments ERROR: %s", r)
			lygo_ext_logs.Error(msg)
			fmt.Println("Scanner.MatchDocuments ERROR: ", msg)
		}
	}()

	stopwatch := lygo_stopwatch.New()
	stopwatch.Start()

	params := new(ScannerConfigArray)
	params.Add(config)

	response := new(ScannerResponse)
	response.Params = params
	response.Documents = make([]*ScannerDocument, 0)
	response.ElapsedMs = 0

	documents, err := instance.SplitDocuments(fileName, oneDocumentPerPage)
	if nil == err {
		for _, documentPages := range documents {
			document := NewScannerDocument(instance.Async, instance.Workspace, documentPages, *config)
			response.Documents = append(response.Documents, document)
		}
	}

	stopwatch.Stop()
	response.ElapsedMs = stopwatch.Milliseconds()

	return response, err
}

func (instance *Scanner) Remove() error {
	dir := instance.Workspace
	return os.RemoveAll(dir)
}

func (instance *Scanner) DebugTextDocument(documentPages []string,
	configArray *ScannerConfigArray) (*ScannerResponse, error) {

	stopwatch := lygo_stopwatch.New()
	stopwatch.Start()

	workConfig := configArray.Clone()

	response := new(ScannerResponse)
	response.Uid = instance.Id
	response.Original = ""
	response.Params = configArray
	response.Documents = make([]*ScannerDocument, 0)
	response.ElapsedMs = 0

	var bestScore float32
	var bestDocument *ScannerDocument
	var emptyDocument *ScannerDocument
	var fallbackDocument *ScannerDocument
	emptyDocument = NewScannerDocumentEmpty(instance.Workspace, documentPages, *new(ScannerConfig))

	// loop on all configurations to get the best match
	for _, config := range workConfig.Items() {
		document := NewScannerDocumentDebug(instance.Async, instance.Workspace, documentPages, config)
		document.Start()

		fallbackDocument = document
		if len(document.Pages) > 0 {
			bestItem := document.BestJobArea()
			if nil != bestItem && nil != bestItem.Nlp {
				if bestItem.Nlp.Score > bestScore {
					bestScore = bestItem.Nlp.Score
					bestDocument = document
				}
			}
		}
	}

	if nil != bestDocument {
		// assign a document to response
		response.Documents = append(response.Documents, bestDocument)
		// matched configuration cannot be removed
		// workConfig.Remove(bestDocument.Params)
	} else if nil != fallbackDocument {
		// not a best document, but something with parameters and data
		response.Documents = append(response.Documents, fallbackDocument)
	} else {
		// document not matching models passed in configuration "configArray"
		response.Documents = append(response.Documents, emptyDocument)
	}

	stopwatch.Stop()
	response.ElapsedMs = stopwatch.Milliseconds()

	return response, nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
func (instance *Scanner) pool() *lygo_async.ConcurrentPool {
	if nil == instance.threadPool {
		instance.threadPool = lygo_async.NewConcurrentPool(10) // max 10 concurrent threads
	}
	return instance.threadPool
}

func (instance *Scanner) copyToWorkspace(source string) (string, error) {
	target := filepath.Join(instance.Workspace, filepath.Base(source))
	_, err := lygo_io.CopyFile(source, target)
	return target, err
}

func (instance *Scanner) buildDocumentFileName(original string, newExtension string) string {
	base := filepath.Base(original)
	if len(newExtension) > 0 {
		base = strings.Replace(base, lygo_paths.Extension(base), newExtension, 1)
	}
	return filepath.Join(instance.Workspace, "original", base)
}

func (instance *Scanner) copyToPage(source string) (string, error) {
	name := lygo_paths.ChangeFileNameWithSuffix(filepath.Base(source), "-0")
	target := filepath.Join(instance.Workspace, "original", name)
	_, err := lygo_io.CopyFile(source, target)
	return target, err
}

func (instance *Scanner) convert(original string) (bool, []string, error) {
	path := lygo_paths.Absolute(original)
	ext := lygo_paths.Extension(path)
	if len(ext) == 0 {
		return false, nil, errors.New("missing file extension")
	}

	format := instance.ConversionFormat
	dpi := instance.ConversionDpi

	if strings.ToLower(ext) == ".pdf" {
		targetPath := instance.buildDocumentFileName(path, "."+format)
		params := lygo_images.NewImageConvertParams()
		params.Source = path
		params.Target = targetPath
		params.Format = format
		params.YRes = dpi
		params.XRes = dpi

		pages, err := lygo_images.Convert(params)

		return true, pages, err
	} else {
		// not converted & no error
		return false, nil, nil
	}
}

func (instance *Scanner) goScanGroup(group *sync.WaitGroup, task *ScannerTask, documentPages []string, config ScannerConfig) {
	if nil != group {
		defer group.Done()
	}
	instance.goScan(task, documentPages, config)
}

func (instance *Scanner) goScan(task *ScannerTask, documentPages []string, config ScannerConfig) {
	workspace := instance.Workspace

	// scan the document
	document := NewScannerDocument(instance.Async, workspace, documentPages, config)
	document.Start()

	if nil != document {
		defer instance.mutex.Unlock()
		instance.mutex.Lock()
		task.Processed = append(task.Processed, document)
	}
}
