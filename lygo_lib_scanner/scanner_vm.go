package lygo_lib_scanner

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_ext_scripting"
	"errors"
)

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t
//----------------------------------------------------------------------------------------------------------------------

const VARIABLE_PREFIX = "VAR_"

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type ScannerVM struct {
	Workspace string

	name string
	program string
}

//----------------------------------------------------------------------------------------------------------------------
//	c o n s t r u c t o r
//----------------------------------------------------------------------------------------------------------------------

func NewScannerVM(filename string) *ScannerVM {
	instance := new(ScannerVM)
	if text, err := lygo_io.ReadTextFromFile(filename); nil == err {
		instance.program = text
		instance.name = lygo_paths.FileName(filename, false)
	}
	instance.Workspace = "./"

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ScannerVM) Eval(context map[string]interface{}) (interface{}, error) {
	if nil != instance && len(instance.program) > 0 {
		runtime := instance.runtime(context)

		response, err := runtime.RunString(instance.program)
		if nil != err {
			return nil, err
		}
		if nil == response {
			return nil, nil
		}
		return response.Export(), nil
	}
	return nil, errors.New("nothing_to_run")
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *ScannerVM) runtime(context map[string]interface{}) *lygo_ext_scripting.ScriptEngine {
	runtime := lygo_ext_scripting.New()
	runtime.Root = instance.Workspace
	runtime.Name = instance.name

	// init global context as text
	// runtime.SetToolsContext(instance)

	if nil != context {
		for k, v := range context {
			if nil != v && nil == runtime.Get(k) {
				runtime.Set(VARIABLE_PREFIX+k, v)
			}
		}
	}

	return runtime
}
