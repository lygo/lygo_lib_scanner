package lygo_lib_scanner

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type ScannerPage struct {
	Id       int                          `json:"id"`
	FileName string                       `json:"filename"`
	Jobs     [_MAX_ROTATE]*ScannerPageJob `json:"jobs"`
}

type ScannerPageJob struct {
	Index    int                   `json:"index"` // page index
	Parent   *ScannerPage          `json:"_"`
	FileName string                `json:"filename"`
	Elapsed  int                   `json:"elapsed"`
	Error    error                 `json:"error"`
	Areas    []*ScannerPageJobArea `json:"areas"`
}

type ScannerPageJobArea struct {
	Index       int                            `json:"index"` // position index in areas array
	Parent      *ScannerPageJob                `json:"_"`
	Uid         string                         `json:"uid"`
	FileName    string                         `json:"filename"`
	Text        string                         `json:"text"`
	IsFullPage  bool                           `json:"is_full_page"`
	Coordinates *ScannerPageJobAreaCoordinates `json:"coordinates"`
	Error       error                          `json:"error"`
	Nlp         *ScannerPageJobAreaNlpResponse `json:"nlp"`
}

type ScannerPageJobAreaCoordinates struct {
	X      int  `json:"x"`
	Y      int  `json:"y"`
	Width  uint `json:"width"`
	Height uint `json:"height"`
}

type ScannerPageJobAreaNlpResponse struct {
	Parent          *ScannerPageJobArea
	Score           float32                  `json:"score"`
	Elapsed         int                      `json:"elapsed"`
	Entities        map[string][]interface{} `json:"entities"`
	IntentEntityUid string                   `json:"intent_entity_uid"`
	IntentUid       string                   `json:"intent_uid"`
}

//----------------------------------------------------------------------------------------------------------------------
//	ScannerPage
//----------------------------------------------------------------------------------------------------------------------

func (instance *ScannerPage) HasIntent() bool {
	for _, job := range instance.Jobs {
		if nil != job {
			if nil != job.Areas {
				for _, item := range job.Areas {
					if nil != item && nil != item.Nlp {
						if len(item.Nlp.IntentUid) > 0 {
							return true
						}
					}
				}
			}
		}
	}
	return false
}

func (instance *ScannerPage) IntentUid() string {
	for _, job := range instance.Jobs {
		if nil != job {
			if nil != job.Areas {
				for _, item := range job.Areas {
					if nil != item && nil != item.Nlp {
						if len(item.Nlp.IntentUid) > 0 {
							return item.Nlp.IntentUid
						}
					}
				}
			}
		}
	}
	return ""
}

// Return best scanner elaboration (nlp matching with image transformation) in a page
func (instance *ScannerPage) BestJob() *ScannerPageJob {
	var response *ScannerPageJob
	var score float32
	score = -1.0
	for _, job := range instance.Jobs {
		if nil != job {
			if nil != job.Areas {
				for _, item := range job.Areas {
					if nil != item && nil != item.Nlp {
						if item.Nlp.Score > score {
							score = item.Nlp.Score
							response = job
						}
					}
				}
			}
		}
	}
	return response
}

func (instance *ScannerPage) Entities() map[string][]interface{} {
	var response map[string][]interface{}
	response = make(map[string][]interface{})
	job := instance.BestJob()
	if nil != job {
		for _, item := range job.Areas {
			if nil != item && nil != item.Nlp {
				if item.Nlp.Score > 0 {
					// copy values
					for k, v := range item.Nlp.Entities {
						response[k] = append(response[k], v...)
					}
				}
			}
		}
	}

	return response
}

//----------------------------------------------------------------------------------------------------------------------
//	ScannerPageJob
//----------------------------------------------------------------------------------------------------------------------

func (instance *ScannerPageJob) Entities(minScore float32) map[string][]interface{} {
	var response map[string][]interface{}
	response = make(map[string][]interface{})
	if len(instance.Areas) > 0 {
		for _, item := range instance.Areas {
			if nil != item && nil != item.Nlp {
				if item.Nlp.Score >= minScore {
					// copy values
					for k, v := range item.Nlp.Entities {
						response[k] = append(response[k], v...)
					}
				}
			}
		}
	}
	return response
}
